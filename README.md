# Ruje

A hastily conceived and poorly thought out escape plan from the world of BibTeX.

[BibTeX](http://www.bibtex.org/) is quite an unpleasant tool to use for
reference management, but still remains the standard in academia. This
repository contains a tool that does a minimal job of translating back-and-forth
between BibTeX and an internal format (currently [JSON](http://json.org/)) that
is much more pleasant to work in. The result is a more modern environment for
maintaining your references (hello, UTF-8!) without losing interoperability
with tools that require BibTeX input.

Existing comments and documentation are minimal. If you want to use this, try
`ruje --help` or send me an email.

## Auto-complete functionality

The sub-directory, youcompleteme, contains a custom completer for
[YouCompleteMe](https://github.com/Valloric/YouCompleteMe). Usage is a bit
awkward at the moment, but essentially you can enable it with the following:

1. Copy the `mkd` directory into the `completers` directory of your
   YouCompleteMe installation. Unfortunately YouCompleteMe doesn't support
   out-of-tree custom completers.

2. Add the following to your `~/.vimrc`:

```vim
" Make YCM trigger a completion request on typing '@'.
let g:ycm_semantic_triggers = {
  \  'mkd':['@'],
  \ }
```

3. Make `ruje` available to Python. The easiest way is to ensure this directory
   itself is in your `PYTHONPATH` environment variable.

4. Create a file `.ruje.conf` in the directory you want to work with the
   following contents:

```dosini
[arguments]
file = /path/to/your/reference/database
```

5. Open a markdown file and start working.

If everything went smoothly, you should get completion suggestions whenever you
type "@".

As I said, this plugin is pretty rough around the edges. If you have
difficulties or suggestions for improvements, please let me know.

## Legal

All code in this repository is in the public domain. Do whatever you like with
it.
