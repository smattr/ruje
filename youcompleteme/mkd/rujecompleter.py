import os, traceback

# In developing this plugin, I found YouCompleteMe quite counterintuitive and
# full of gotchas. The following two functions are provided to ease associated
# debugging pain if you are developing or troubleshooting this plugin. If you
# want full exception introspection and more detailed output options, create a
# FIFO on disk and set the environment variable RUJECOMPLETER_DEBUG_PIPE to its
# path.

_pynotify = None
def _debug_notify(message):
    global _pynotify
    if _pynotify is None:
        import pynotify
        pynotify.init('RujeCompleter')
        _pynotify = pynotify
    n = _pynotify.Notification(message)
    n.show()

DEBUG_PIPE = os.environ.get('RUJECOMPLETER_DEBUG_PIPE')
def _debug_print(message):
    if DEBUG_PIPE is not None:
        with open(DEBUG_PIPE, 'a') as f:
            print >>f, message

try:
    from ycm.completers.completer import Completer
    from ycm.server import responses
    import ycm_client_support
except ImportError as e:
    _debug_print('Exception: %s' % e)
    _debug_print(traceback.format_exc())
    raise

# Test YCM is the version this code was developed against.
try:
    version = ycm_client_support.YcmCoreVersion()
    if version != 8:
        raise Exception('warning: YCM reports core version %s, not 8 as expected' % version)
except Exception as e:
    _debug_print('Exception: %s' % e)
    _debug_print(traceback.format_exc())
    raise

try:
    import ruje
except ImportError:
    # If Ruje is not available, just don't perform auto-completion.
    ruje = None

def to_bibtex(key, value):
    '''
    Turn a result from Ruje into a bibtex entry for display in YouCompleteMe's
    API box.
    '''
    assert isinstance(key, basestring)
    assert isinstance(value, dict)
    return '@%(type)s{%(key)s,' \
           '%(entries)s\n' \
           '}' % {
               'type':value.get('type', 'unknown'),
               'key':key,
               'entries':''.join('\n  %s = {%s}' % (k, v)
                    for k, v in value.items() if k not in ('key', 'type')),
           }

# The actual completer itself follows. Note that functions are denser than they
# would otherwise be as they are wrapped in debugging helpers.

class RujeCompleter(Completer):
    def __init__(self, user_options):
        try:

            super(RujeCompleter, self).__init__(user_options)
            # If Ruje is available and we have a local configuration, load the
            # associated database.
            conf = os.path.join(os.getcwd(), '.ruje.conf')
            if ruje is not None and os.path.exists(conf):
                opts = ruje.parse_config(conf)
                self.refs = ruje.Database(opts.file)
            else:
                self.refs = None

        except Exception as e:
            _debug_print('Exception: %s' % e)
            _debug_print(traceback.format_exc())
            raise

    def SupportedFiletypes(self):
        try:
            if ruje is None:
                # For efficiency, tell YCM we support nothing if we won't
                # actually be able to provide any completions.
                return []
            return ['mkd']
        except Exception as e:
            _debug_print('Exception: %s' % e)
            _debug_print(traceback.format_exc())
            raise

    def ComputeCandidatesInner(self, request_data):
        _debug_print(str(request_data))
        try:
            if self.refs is None:
                # Ruje unavailable.
                return []

            # First find the stem currently under the cursor.
            line = request_data['line_value']
            col = request_data['column_num']
            start_index = line.rfind('@', 0, col)
            if start_index == -1:
                # Didn't find a '@'; not sure why we were triggered.
                return []
            stem = line[start_index + 1:col]

            # Now look up matching references.
            refs = self.refs.search(stem)

            # Return them with the full reference dict in the preview pane.
            return [responses.BuildCompletionData(k, v['title'], to_bibtex(k, v))
                for k, v in refs.items()]

        except Exception as e:
            _debug_print('Exception: %s' % e)
            _debug_print(traceback.format_exc())
            raise
