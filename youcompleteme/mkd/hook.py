from ycm.completers.mkd.rujecompleter import RujeCompleter

def GetCompleter(user_options):
    return RujeCompleter(user_options)
